class TasksController < ApplicationController

  def show_by_person
    person = Person.find(params[:id])
    tasks = person.tasks
    render json: [person.name, "Sus tareas son", tasks]
  end

end

