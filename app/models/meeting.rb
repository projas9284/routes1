class Meeting < ActiveRecord::Base
	has_many :invitations
	has_many :people, :through => :invitations
	has_many :points
	has_many :agreements, :through => :points
	has_many :tasks, :through => :points
end
