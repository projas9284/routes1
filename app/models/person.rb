class Person < ActiveRecord::Base
	has_many :invitations
	has_many :meetings, :through => :invitations
	has_many :assignments
	has_many :tasks, :through => :assignments
end
