class Point < ActiveRecord::Base
	belongs_to :meeting
	has_many :agreements
	has_many :tasks
end
